#version 310 core

in vec3 vertex_position;

void main(){
    gl_position.xyz=vertex_position;
    gl_position.w=1.;
}