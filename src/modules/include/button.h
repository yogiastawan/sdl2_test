#ifndef __G_BUTTON_H__
#define __G_BUTTON_H__

#include <functional>
#include <vector>
#include <string>

#include <gmodule.h>

namespace gihex
{
    class Button : public Module
    {
    private:
        enum ButtonState
        {
            NONE,
            HOVER
        };

        ButtonState state;
        bool prevent_click = false;
        bool is_hidden = false;
        bool is_hover = false;
        std::function<void()> on_click_f;
        GSize m_size = {100, 50};
        GSize m_win_size = {0, 0};
        GPosition m_pos = {0, 0};

        bool hover(i32 mouse_x, i32 mouse_y);
        void change_color(color clr);
        void change_vertex_pos();

        inline f32 x_gl_unit(u32 numb)
        {
            return ((2.f * (f32)numb) - (f32)m_win_size.w) / (f32)m_win_size.w;
        }
        inline f32 y_gl_unit(u32 numb)
        {
            return ((f32)m_win_size.h - (2.0f * (f32)numb)) / (f32)m_win_size.h;
        }

        u32 vao;
        u32 vbo[2];
        u32 shader_program;

        color def = color(0.3f, 0.3f, 0.3f, 1.f);
        color hov = color(0.6f, 0.6f, 0.6f, 1.f);

        std::vector<color> bg{std::vector<color>(6, def)};

        std::string vert = R"(#version 330 core

in vec2 vertex_position;
in vec4 colors;

out vec4 color;
void main(){
    gl_Position.xy=vertex_position;
    gl_Position.z=0.;
    gl_Position.w=1.;
    color=colors;
})";

        std::string frag = R"(#version 330 core

in vec4 color;
void main(){
    gl_FragColor=color;
})";
        std::vector<f32> vertex_position;

    public:
        Button(SDL_Window *win);
        ~Button();

        void event(SDL_Event &event);
        void update();
        void draw();
        void on_leave_context();
        void on_enter_context();

        void set_position(u32 x, u32 y);
        
        void set_size(u32 w, u32 h);

        void set_color(color clr);
        void set_color(f32 r, f32 g, f32 b, f32 a);
        void set_hover_color(color clr);
        void set_hover_color(f32 r, f32 g, f32 b, f32 a);

        void on_click(std::function<void()> callback);
    };

} // namespace gihex

#endif /*__G_BUTTON_H__*/