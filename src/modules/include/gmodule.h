#ifndef __GIHEX_MODULE_H__
#define __GIHEX_MODULE_H__

#include <SDL.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <gtype.h>

namespace gihex
{
    class Module
    {
    private:
        /* data */
    protected:
        bool context;

    public:
        Module(/* args */);
        virtual ~Module();

        virtual void event(SDL_Event &event);
        virtual void update();
        virtual void draw();
        virtual void on_leave_context();
        virtual void on_enter_context();
    };

} // namespace gihex

#endif /*__G_MODULE_H__*/