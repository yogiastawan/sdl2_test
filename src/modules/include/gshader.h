#ifndef __G_SHADER_H__
#define __G_SHADER_H__

#include <string>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <gtype.h>

#include <SDL.h>

namespace gihex
{
    u32 create_shader(std::string vertex, std::string fragment)
    {
        u32 vt = glCreateShader(GL_VERTEX_SHADER);
        u32 ft = glCreateShader(GL_FRAGMENT_SHADER);

        // compile vertex
        char const *vt_code = vertex.c_str();
        glShaderSource(vt, 1, &vt_code, nullptr);
        glCompileShader(vt);
        i32 state_compile;
        GLchar vertex_error[1024] = {0};
        glGetShaderiv(vt, GL_COMPILE_STATUS, &state_compile);
        if (state_compile == GL_FALSE)
        {
            glGetShaderInfoLog(vt, sizeof(vertex_error), nullptr, vertex_error);
            SDL_Log("Error compiling vertex shader: %s", vertex_error);
            return 0;
        }

        char const *ft_code = fragment.c_str();
        glShaderSource(ft, 1, &ft_code, nullptr);
        glCompileShader(ft);
        glGetShaderiv(ft, GL_COMPILE_STATUS, &state_compile);
        if (state_compile == GL_FALSE)
        {
            glGetShaderInfoLog(ft, sizeof(vertex_error), nullptr, vertex_error);
            SDL_Log("Error compiling fragment shader: %s", vertex_error);
            return 0;
        }

        u32 program = glCreateProgram();

        // link program
        glAttachShader(program, vt);
        glAttachShader(program, ft);
        glLinkProgram(program);

        i32 state_program;
        GLchar program_error[1024] = {0};
        glGetProgramiv(program, GL_LINK_STATUS, &state_program);
        if (state_program == GL_FALSE)
        {
            glGetProgramInfoLog(program, sizeof(program_error), nullptr, program_error);
            SDL_Log("Error link program: %s", program_error);
            return 0;
        }

        glDetachShader(program, vt);
        glDetachShader(program, ft);
        glDeleteShader(vt);
        glDeleteShader(ft);

        return program;
    }
} // namespace gihex

#endif /*__G_SHADER_H__*/