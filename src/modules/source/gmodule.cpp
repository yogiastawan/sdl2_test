#include <gmodule.h>

gihex::Module::Module()
{
    on_enter_context();
}

gihex::Module::~Module()
{
    on_leave_context();
    SDL_Log("Destroy Module");
}

void gihex::Module::event(SDL_Event &event)
{
}

void gihex::Module::update()
{
}

void gihex::Module::draw()
{
}

void gihex::Module::on_leave_context()
{
    context = false;
}

void gihex::Module::on_enter_context()
{
    SDL_Log("start-context");
    context = true;
}
