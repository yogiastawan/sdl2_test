#include <button.h>
#include <gshader.h>

bool gihex::Button::hover(i32 mouse_x, i32 mouse_y)
{
    // SDL_GetMouseState(&mouse_x, &mouse_y);
    // SDL_Log("%d,%d", mouse_x, mouse_y);
    u32 padd = 1;
    return is_hover = ((mouse_x + padd - 1 - m_pos.x) < (m_size.w - padd)) && ((mouse_y + padd - 1 - m_pos.y) < (m_size.h - padd));
}

void gihex::Button::change_color(color clr)
{
    bg.clear();
    bg = std::vector<color>(6, clr);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * bg.size() * clr.length(), bg.data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void gihex::Button::change_vertex_pos()
{
    vertex_position.clear();
    f32 vx = x_gl_unit(m_pos.x);
    f32 vy = y_gl_unit(m_pos.y);
    f32 vw = x_gl_unit(m_pos.x + m_size.w);
    f32 vh = y_gl_unit(m_pos.y + m_size.h);
    vertex_position = {
        vx, vy,
        vx, vh,
        vw, vy,
        vw, vy,
        vx, vh,
        vw, vh};
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * vertex_position.size(), vertex_position.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

gihex::Button::Button(SDL_Window *win) : Module()
{
    i32 w, h;
    SDL_GetWindowSize(win, &w, &h);
    m_win_size = {(u32)w, (u32)h};
    f32 vx = x_gl_unit(m_pos.x);
    f32 vy = y_gl_unit(m_pos.y);
    f32 vw = x_gl_unit(m_pos.x + m_size.w);
    f32 vh = y_gl_unit(m_pos.y + m_size.h);
    vertex_position = {
        vx, vy,
        vx, vh,
        vw, vy,
        vw, vy,
        vx, vh,
        vw, vh};

    // create 1 vao
    glGenVertexArrays(1, &vao);
    // create 2 vbo (vetrex position and vertex color)
    glGenBuffers(2, &vbo[0]);

    // create shader program
    shader_program = create_shader(vert, frag);

    if (!shader_program)
        return;
    glUseProgram(shader_program);

    glBindVertexArray(vao);
    // position
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * vertex_position.size(), vertex_position.data(), GL_STATIC_DRAW);
    i32 pos = glGetAttribLocation(shader_program, "vertex_position");
    glEnableVertexAttribArray(pos);
    glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    // color
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * bg.size() * 4, bg.data(), GL_DYNAMIC_DRAW);
    i32 pos1 = glGetAttribLocation(shader_program, "colors");
    glEnableVertexAttribArray(pos1);
    glVertexAttribPointer(pos1, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

    // unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

gihex::Button::~Button()
{
    SDL_Log("Destroy Button");
}

void gihex::Button::event(SDL_Event &event)
{
    if (is_hidden)
        return;

    if (event.type == SDL_MOUSEMOTION)
    {
        if (!hover(event.motion.x, event.motion.y))
        {
            // SDL_Log("not hovered");
            if (state == NONE)
                return;
            state = NONE;
            change_color(def);
            return;
        }
        // SDL_Log("hovered");
        if (state == HOVER)
            return;
        state = HOVER;
        change_color(hov);
    }
    else if (event.type == SDL_MOUSEBUTTONUP)
    {
        if (prevent_click)
        {
            prevent_click = false;
            return;
        }
        if (!is_hover)
            return;

        if (!on_click_f)
            return;
        // SDL_Log("clicked");

        // left click
        if (event.button.button == SDL_BUTTON_LEFT)
        {
            // is_hover = false;
            on_click_f();
        }
    }
}

void gihex::Button::update()
{
    if (is_hidden)
        return;
    // SDL_Log("button update");
}

void gihex::Button::draw()
{
    if (is_hidden)
        return;

    // draw button
    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES, 0, vertex_position.size() / 2);
    glBindVertexArray(0);
}

void gihex::Button::on_leave_context()
{
    is_hover = false;
    state = NONE;
    gihex::Module::on_leave_context();
}

void gihex::Button::on_enter_context()
{
    gihex::Module::on_enter_context();
    i32 x, y;
    SDL_GetMouseState(&x, &y);

    if (hover(x, y))
    {
        prevent_click = true;
        change_color(hov);
        return;
    }
    change_color(def);
}

void gihex::Button::set_position(u32 x, u32 y)
{
    m_pos = {x, y};
    change_vertex_pos();
}

void gihex::Button::set_size(u32 w, u32 h)
{
    m_size = {w, h};
    change_vertex_pos();
}

void gihex::Button::set_color(color clr)
{
    def = clr;
    change_color(def);
}

void gihex::Button::set_color(f32 r, f32 g, f32 b, f32 a)
{
    def = color(r, g, b, a);
    change_color(def);
}

void gihex::Button::set_hover_color(color clr)
{
    hov = clr;
}

void gihex::Button::set_hover_color(f32 r, f32 g, f32 b, f32 a)
{
    hov = color(r, g, b, a);
}

void gihex::Button::on_click(std::function<void()> callback)
{
    on_click_f = callback;
}