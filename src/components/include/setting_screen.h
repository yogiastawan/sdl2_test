#ifndef __SETTING_SCREEN_H__
#define __SETTING_SCREEN_H__

#include <memory>

#include <engine.h>
#include <screen.h>

namespace gihex
{
    class SettingScreen : public Screen
    {

    private:
    public:
        SettingScreen(std::weak_ptr<Engine> engine);
        ~SettingScreen();

        void event(SDL_Event &ev);
        void update();
        void render();
    };

} // namespace gihex

#endif /*__SETTING_SCREEN_H__*/