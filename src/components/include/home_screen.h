#ifndef __G_HOME_SCREEN_H__
#define __G_HOME_SCREEN_H__

#include <screen.h>
#include <functional>
#include <memory>

#include <engine.h>
#include <button.h>

namespace gihex
{
    // class Engine;
    class HomeScreen : public Screen
    {
    private:
        void ui_create();

    public:
        HomeScreen(std::shared_ptr<Engine> engine);
        void event(SDL_Event &ev);
        // void update();
        void render();
        ~HomeScreen();
    };

} // namespace gihex

#endif /*__G_HOME_SCREEN_H__*/