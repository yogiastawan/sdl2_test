#include <setting_screen.h>

gihex::SettingScreen::SettingScreen(std::weak_ptr<Engine> engine) : Screen(engine)
{
}

gihex::SettingScreen::~SettingScreen()
{
    SDL_Log("destroy setting screen...");
}

void gihex::SettingScreen::event(SDL_Event &ev)
{
    if (ev.type == SDL_MOUSEBUTTONUP)
    {
        eng.lock()->get_screen_manager()->go_back();
    }
}

void gihex::SettingScreen::update()
{
    // SDL_Log("setting update");
}

void gihex::SettingScreen::render()
{
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
