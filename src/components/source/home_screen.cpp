#include <home_screen.h>

#include <setting_screen.h>
#include <button.h>
#include <iterator>

void gihex::HomeScreen::ui_create()
{

    std::shared_ptr<Button> button_start = std::make_shared<Button>(eng.lock()->get_window());
    std::weak_ptr<Button> bs(button_start);
    std::weak_ptr<Engine> en(eng);
    button_start->on_click([bs, en]()
                           {
                               //    en.lock()->exit(); /work
                               //   try change button color;
                               //    bs.lock()->set_color(0.f, 1.0f, 1.0f, 1.0f); // work
                               SettingScreen *scr = new SettingScreen(en);
                               en.lock()->get_screen_manager()->start_screen(scr);
                               //    end
                           });
    add_module(button_start);
    std::shared_ptr<Button> button2 = std::make_shared<Button>(Button(eng.lock()->get_window()));
    button2->set_position(100, 300);
    button2->set_size(200, 300);
    u32 x = 20;
    f32 c = 0.0f;
    button2->on_click([bs, x, c]() mutable
                      { bs.lock()->set_position(x, 0);
                      bs.lock()->set_color(c, 0.5f, 1.0f, 1.0f);
                      x+=20;
                      c+=0.01; });
    add_module(button2);
    std::shared_ptr<Button> button_add = std::make_shared<Button>(eng.lock()->get_window());
    button_add->set_position(20, 600);
    button_add->set_size(200, 100);
    u32 post = 0;
    // std::weak_ptr<std::list<std::shared_ptr<Module>>> cmp(components);
    // fix thizzzzz
    button_add->on_click([this, post, en]() mutable
                         {
                            SDL_Log("Add Button  %d",this->get_module_size());
                             std::shared_ptr<Button> a = std::make_shared<Button>(Button(en.lock()->get_window()));
                             a->set_position(post,700);
                             a->set_size(80,40);
                            this->add_module(a);
                            //cmp.lock()->emplace_back(a);//fix me
                            //cmp.lock()->insert(cmp.lock()->end(),a);
                            //cmp.lock()->push_back(a);
                             SDL_Log("size cmp: %d",this->get_module_size());
                             post+=90; });
    add_module(button_add);

    std::shared_ptr<Button> button_show = std::make_shared<Button>(eng.lock()->get_window());
    button_show->set_position(240, 600);
    button_show->set_size(200, 100);
    button_show->set_color(0.f, 1.f, 1.f, 1.f);
    button_show->on_click([this]()
                          { SDL_Log("size cmp: %d", this->get_module_size()); });
    add_module(button_show);
    SDL_Log("size comp: %d", get_module_size());
}

gihex::HomeScreen::HomeScreen(std::shared_ptr<Engine> engine) : Screen(engine)
{
    eng = engine;
    ui_create();
}

// overide event
void gihex::HomeScreen::event(SDL_Event &ev)
{
    gihex::Screen::event(ev);
}

// overide update
//  void gihex::HomeScreen::update()
//  {
//      // TODO! update home
//      // SDL_Log("home update");
//  }

// overide render
void gihex::HomeScreen::render()
{
    // TODO! render home
    // SDL_GL_MakeCurrent(win, ctx);
    glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // call parent to draw modules
    gihex::Screen::render();
}

gihex::HomeScreen::~HomeScreen()
{
    SDL_Log("destroy home screen...");
    eng.reset();
}
