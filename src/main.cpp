#include <iostream>

#include <SDL.h>
#include <engine.h>
#include <screen.h>
#include <home_screen.h>
#include <setting_screen.h>
#include <memory>
int main(int, char **)
{

    std::shared_ptr<gihex::Engine> a(gihex::Engine::instance());
    if (!a->init())
    {
        std::cout << "Cannot starting engine" << std::endl;
        return -1;
    }

    // using this method, inside Screen Manager scr.use_count=2. use this when you only use scr outside
    // std::shared_ptr<gihex::HomeScreen> scr=std::make_shared<gihex::HomeScreen>(gihex::HomeScreen(a));
    // a->get_screen_manager()->start_screen(scr);

    // use_count=1;
    a->get_screen_manager()->start_screen(std::make_shared<gihex::HomeScreen>(gihex::HomeScreen(a)));

    a->run();

    return 0;
}
