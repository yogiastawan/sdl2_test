#ifndef __G_SCREEN_H__
#define __G_SCREEN_H__

#include <memory>
#include <list>

#include <SDL.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <engine.h>
#include <gmodule.h>

namespace gihex
{

    class Engine;

    class Screen
    {
    private:
        std::shared_ptr<std::list<std::shared_ptr<Module>>> components = std::make_shared<std::list<std::shared_ptr<Module>>>();

    protected:
        bool screen_ctx;
        std::weak_ptr<Engine> eng;

    public:
        Screen(std::weak_ptr<Engine> engine);
        virtual ~Screen();

        virtual void event(SDL_Event &event);
        virtual void update();
        virtual void render();
        virtual void create_context();
        virtual void release_context();

        void start_event_components(SDL_Event &ev);
        void start_draw_components();

        void add_module(std::shared_ptr<Module> ui);
        size_t get_module_size() const;
    };
}

#endif /*__G_SCREEN_H__*/