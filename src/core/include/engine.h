#ifndef __G_ENGINE__
#define __G_ENGINE__

#include <SDL.h>
#include <vector>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <gtype.h>

#include <screen_manager.h>
#include <screen.h>

namespace gihex
{
    enum FPS
    {
        FPS_30,
        FPS_60,
        FPS_90,
        FPS_120
    };

    enum VSYNC
    {
        VSYNC_DISABLE,
        VSYNC_ENABLE,
    };

    class Screen;
    class ScreenManager;

    class Engine
    {
    private:
        Engine();

        static Engine *engine;
        SDL_Window *win;
        SDL_GLContext ctx;
        bool is_running;
        u32 fps;
        bool vsync;

        SDL_DisplayMode display_mode;
        u8 numb_display = 0;

        std::shared_ptr<ScreenManager> sm;

    public:
        ~Engine();
        bool init();
        static Engine *instance();
        void run();
        void set_FPS(FPS fps);
        void set_VSync(VSYNC vsync);
        void exit();

        inline SDL_Window *get_window() const
        {
            return win;
        }
        inline SDL_GLContext get_gL_ctx() const
        {
            return ctx;
        }
        std::shared_ptr<ScreenManager> get_screen_manager();
        inline void quit()
        {
            is_running = false;
        }
    };

}

#endif /*__G_ENGINE__*/