#ifndef __GIHEX_TYPE_H__
#define __GIHEX_TYPE_H__

#include <glm/glm.hpp>

typedef long i64;
typedef unsigned long u64;
typedef int i32;
typedef unsigned int u32;
typedef short i16;
typedef unsigned short u16;
typedef char i8;
typedef unsigned char u8;

typedef float f32;
typedef double f64;

namespace gihex
{

    struct GPosition
    {
        u32 x;
        u32 y;
    };

    struct GSize
    {
        u32 w;
        u32 h;
    };

} // namespace gihex

typedef glm::vec4 color;

#endif /*__GIHEX_TYPE_H__*/