#ifndef __G_SCREEN_MANAGER_H__
#define __G_SCREEN_MANAGER_H__

#include <vector>
#include <memory>

#include <screen.h>
#include <gtype.h>

namespace gihex
{
    enum ScreenManagerState
    {
        EMPTY,
        FILLED
    };

    class Screen;

    class ScreenManager
    {
    private:
        std::vector<std::shared_ptr<Screen>> screens;
        u8 currrentIndex;
        ScreenManagerState state = EMPTY;

    public:
        ScreenManager();
        ~ScreenManager();

        void start_screen(Screen *screen);
        void start_screen(std::shared_ptr<Screen> screen);
        void replace_current_screen(Screen *screen);
        void replace_current_screen(std::shared_ptr<Screen> screen);
        void run(SDL_Event &event);
        void draw_screen();
        void go_back();
        inline ScreenManagerState get_state() const
        {
            return state;
        }
    };
} // namespace gihex

#endif /*__G_SCREEN_MANAGER_H__*/