#include <screen.h>

gihex::Screen::Screen(std::weak_ptr<Engine> engine)
{
    eng = engine;
    create_context();
}

gihex::Screen::~Screen()
{

    SDL_Log("destroy screen...");
    release_context();
    eng.reset();
}
void gihex::Screen::event(SDL_Event &ev)
{
    start_event_components(ev);
}

void gihex::Screen::update()
{
    // SDL_Log("screen update");
}

void gihex::Screen::render()
{
    start_draw_components();
}

void gihex::Screen::create_context()
{
    screen_ctx = true;
    for (const std::weak_ptr<Module> module : *components)
    {
        module.lock()->on_enter_context();
    }
}

void gihex::Screen::release_context()
{
    screen_ctx = false;
    for (const std::weak_ptr<Module> module : *components)
    {
        module.lock()->on_leave_context();
    }
}

void gihex::Screen::start_draw_components()
{
    for (const std::weak_ptr<Module> module : *components)
    {
        module.lock()->update();
        module.lock()->draw();
    }
}

void gihex::Screen::add_module(std::shared_ptr<Module> ui)
{
    components->emplace_back(ui);
}

size_t gihex::Screen::get_module_size() const
{
    return components->size();
}

void gihex::Screen::start_event_components(SDL_Event &ev)
{

    for (const std::weak_ptr<Module> module : *components)
    {
        module.lock()->event(ev);
    }
}
