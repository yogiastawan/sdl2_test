#include <screen_manager.h>

gihex::ScreenManager::ScreenManager()
{
}

gihex::ScreenManager::~ScreenManager()
{
    SDL_Log("Destroy Screen Manager");
}

void gihex::ScreenManager::start_screen(Screen *screen)
{
    screens.emplace_back(std::shared_ptr<Screen>(screen));
    currrentIndex = screens.size() - 1;
    state = FILLED;
    if (!currrentIndex)
        return;
    screens[currrentIndex - 1]->release_context();
}

void gihex::ScreenManager::start_screen(std::shared_ptr<Screen> screen)
{
    screens.emplace_back(screen);
    currrentIndex = screens.size() - 1;
    state = FILLED;
    if (!currrentIndex)
        return;
    screens[currrentIndex - 1]->release_context();
}

void gihex::ScreenManager::replace_current_screen(Screen *screen)
{
    if (screens.size() <= 0)
    {
        return;
    }
    screens[currrentIndex] = std::shared_ptr<Screen>(screen);
}

void gihex::ScreenManager::replace_current_screen(std::shared_ptr<Screen> screen)
{
    if (screens.size() <= 0)
    {
        return;
    }
    screens[currrentIndex] = screen;
}

void gihex::ScreenManager::run(SDL_Event &event)
{
    // SDL_Log("index: %d", currrentIndex);
    screens[currrentIndex]->event(event);

    // screens[currrentIndex]->show(event);
}

void gihex::ScreenManager::draw_screen()
{
    screens[currrentIndex]->update();
    screens[currrentIndex]->render();
}

void gihex::ScreenManager::go_back()
{
    if (screens.size() <= 1)
    {
        return;
    }
    currrentIndex -= 1;
    screens[currrentIndex]->create_context();
    screens.pop_back();
    SDL_Log("Size: %d", screens.size());
}