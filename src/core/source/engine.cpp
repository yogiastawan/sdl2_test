#include <engine.h>

gihex::Engine *gihex::Engine::engine = nullptr;

bool gihex::Engine::init()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        SDL_Log("Cannot init SDL2: %s", SDL_GetError());
        return false;
    }
    win = SDL_CreateWindow("SDL_test0",
                           SDL_WINDOWPOS_CENTERED,
                           SDL_WINDOWPOS_CENTERED,
                           600, 600,
                           SDL_WINDOW_FULLSCREEN | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_OPENGL);
    if (win == nullptr)
    {
        SDL_Log("Cannot crete Window: %s", SDL_GetError());
        return false;
    }
    ctx = SDL_GL_CreateContext(win);
    numb_display = SDL_GetNumVideoDisplays();
    SDL_Log("numb display: %d", numb_display);
    if (numb_display <= 0)
    {
        SDL_Log("No display found");
        return false;
    }
    SDL_GetCurrentDisplayMode(0, &display_mode);
    // TODO! Add in setting list of display using:
    // for (u8 i = 0; i < numbDisplay; i++)
    // {

    //     modes.push_back(SDL_GetNumDisplayModes(i))
    // }

    return is_running = true;
}

gihex::Engine::Engine()
{
    sm = std::shared_ptr<ScreenManager>(new ScreenManager());
}

gihex::Engine::~Engine()
{
    SDL_Log("destroy engine...");
    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(win);
    SDL_Quit();
}

gihex::Engine *gihex::Engine::instance()
{
    engine = (engine != nullptr) ? engine : new gihex::Engine();
    return engine;
}

void gihex::Engine::run()
{
    SDL_Log("rr: %d, View: %d,%d", display_mode.refresh_rate, display_mode.w, display_mode.h);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_CULL_FACE);
    glViewport(0, 0, display_mode.w, display_mode.h);

    // Atleast must have 1 screen
    if (sm->get_state() == gihex::ScreenManagerState::EMPTY)
    {
        SDL_Log("Couldn't run engine because screen is empty");
        return;
    }

    SDL_Event event;
    while (is_running)
    {
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                is_running = false;
                continue;
            }
            sm->run(event);
        }
        sm->draw_screen();
        SDL_GL_SwapWindow(win);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void gihex::Engine::set_FPS(FPS fp)
{
    fps = 30 * (1 + fp);
}

void gihex::Engine::set_VSync(VSYNC vsy)
{
    vsync = vsy == 1;
}

void gihex::Engine::exit()
{
    is_running = false;
}

std::shared_ptr<gihex::ScreenManager> gihex::Engine::get_screen_manager()
{
    return sm;
}